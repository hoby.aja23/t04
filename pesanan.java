class pesanan extends kue{
    public pesanan(String name, double price,double berat) {
        super(name, price);
        setBerat(berat);
        
    }
    private double weight;
    private double getBerat(){
        return weight;
    }
    private void setBerat(double berat){
        this.weight = berat;
    }
    public double hitungHarga(){
        return super.getPrice()*weight;
    }

    @Override
    public double Berat(){
        return weight;
    }
    @Override
    public double getPrice(){
        return super.getPrice();
    }
    @Override
    public void setPrice(double price){
        super.setPrice(price);
    }
    @Override
    public double Jumlah() {
        return 0;
    }
}